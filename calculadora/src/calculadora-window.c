/* calculadora-window.c
 *
 * Copyright 2020 Leandro Ramos
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "calculadora-config.h"
#include "calculadora-window.h"

struct _CalculadoraWindow
{
  GtkApplicationWindow  parent_instance;

  /* Template widgets */
  GtkHeaderBar        *header_bar;
  GtkLabel            *label;
};

G_DEFINE_TYPE (CalculadoraWindow, calculadora_window, GTK_TYPE_APPLICATION_WINDOW)

static void
calculadora_window_class_init (CalculadoraWindowClass *klass)
{
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);

  gtk_widget_class_set_template_from_resource (widget_class, "/io/github/leandroramos/calculadora/calculadora-window.ui");
  gtk_widget_class_bind_template_child (widget_class, CalculadoraWindow, header_bar);
  gtk_widget_class_bind_template_child (widget_class, CalculadoraWindow, label);
}

static void
calculadora_window_init (CalculadoraWindow *self)
{
  gtk_widget_init_template (GTK_WIDGET (self));
}
