/* teste-window.c
 *
 * Copyright 2020 Leandro Ramos
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <gtk-3.0/gtk/gtk.h>

GtkLabel *label_peso;
GtkLabel *label_altura;
GtkLabel *label_resultado;
GtkEntry *peso;
GtkEntry *altura;

void on_botao_clicked(GtkWidget *widget, gpointer data)
{

}

void on_main_window_destroy(GtkWidget *widget, gpointer data)
{
    gtk_main_quit();
}

int main(int argc, char *argv[])
{
    gtk_init(&argc, &argv);

    GtkBuilder *builder = gtk_builder_new_from_resource ("/io/github/leandroramos/teste/ui.xml");

    gtk_builder_add_callback_symbols(builder,
                                    "on_botao_clicked",
                                    G_CALLBACK(on_botao_clicked),
                                    "on_main_window_destroy",
                                    G_CALLBACK(on_main_window_destroy),
                                    NULL);

    gtk_builder_connect_signals(builder, NULL);

    label_peso = GTK_LABEL(gtk_builder_get_object(builder, "label_peso"));
    label_altura = GTK_LABEL(gtk_builder_get_object(builder, "label_altura"));
    label_resultado = GTK_LABEL(gtk_builder_get_object(builder, "label_resultado"));
    peso = GTK_ENTRY(gtk_builder_get_object(builder, "peso"));
    altura = GTK_ENTRY(gtk_builder_get_object(builder, "altura"));

    GtkWidget *window = GTK_WIDGET(gtk_builder_get_object(builder, "main_window"));

    gtk_widget_show_all(window);

    gtk_main();

    return 0;

}
